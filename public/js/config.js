require.config({
	baseUrl: '/public/js',
	paths: {
		'jquery': 'http://www.ijquery.cn/js/jquery-1.9.0.min',
		'bootstrap': 'http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min',
	},
	shim:{
		'bootstrap': {
			exports: "$",
			deps: ['jquery']
		},
	}
});