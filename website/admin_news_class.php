<?php
	include_once 'admin_global.php';
	$r = $db->Get_user_shell_check($uid, $shell);
	if(isset($_POST['into_class'])){
		$db->query("INSERT INTO `n_newsclass`(`f_id`,`name`,`keyword`,`remark`) VALUES('$_POST[f_id]','$_POST[class_name]','','')");
		$db->Get_admin_msg("admin_news_class.php","成功添加新闻分类");
	}
	if(!empty($_GET['del'])){
		$db->query("DELETE FROM `n_newsclass` WHERE `id`='$_GET[del]'");
		$db->Get_admin_msg("admin_news_class.php","成功删除新闻分类");
	}
	if(isset($_POST['update_class'])){
		$db->query("UPDATE `n_newsclass` set name='$_POST[name]' WHERE id='$_POST[id]'");
		$db->Get_admin_msg("admin_news_class.php","成功更新新闻分类");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>后台管理-新闻分类</title>
		<meta http-equiv=content-type content="text/html; charset=gb2312">
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/jquery-1.9.0.min.js"></script>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet">
		<script src="/public/js/bootstrap.min.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- 菜单 -->
		<?php require_once "admin_menu.php"; ?>
		<!-- 主体 -->
		<div class="table-responsive w98b">  
			<!--panel star-->
			<div class="panel panel-default">
				<div class="panel-heading">
					后台管理 &gt;&gt; 分类管理
				</div>
				<div class="panel-body">
					<form action="" method="post" class="form-inline" >
						<fieldset>
							<legend>填加分类</legend>
							<div class="form-group">
								<label class="sr-only" for="f_id">添加大类</label>
								<select name="f_id" id="f_id" class="form-control ">
									<option value="0">添加大类</option>
									<?php
										$result = $db->findall("`n_newsclass` WHERE `f_id`=0");
										while($row = $db->fetch_array($result)){
											$row_arr[$row['id']] = $row['name'];
											echo "<option value='$row[id]'>".$row['name']."</option>";
										}
									?>
								</select>
							</div>
							<div class="form-group">	
								<label class="sr-only" for="class_name"></label>
								<input type="text" name="class_name" id="class_name" class="form-control" />
							</div>
							<div class="form-group">
								<label class="sr-only" for="into_class">添 加</label>
								<input type="submit" name="into_class" id="into_class"  class="btn btn-default"  value="添 加" />
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<!--panel end-->
			
			<!--panel star-->
			<div class="panel panel-default">
				
				
				<div class="panel-body">
					<?php
						foreach($row_arr as $id=>$name){
						?>
						<form action="" method="post" class="form-inline" >
							<input type="hidden" name="id" value="<?=$id?>" />
							<div class="form-group">	
								<label class="sr-only" for="name"></label>
								<input type="text" name="name" id="name" value="<?=$name?>"  class="form-control"/>
							</div>
							<div class="form-group">	
								<label class="sr-only" for="update_class"></label>
								<input type="submit" name="update_class" id="update_class" value="更 新" class="btn btn-default" />
							</div>
							<div class="form-group">	
								<label class="sr-only" for="del"></label>
								<input type="button" value="删 除" id="del" onclick="location.href='?del=<?=$id?>'" class="btn btn-default" />
							</div>
						</fieldset>
					</form>
					<?php
						$result_fid = $db->findall("`n_newsclass` WHERE `f_id`='$id'");
						while($row_fid=$db->fetch_array()){
						?>
						<form action="" method="post" class="form-inline">
							&nbsp;&nbsp;&nbsp;┗<input type="hidden" name="id" value="<?=$row_fid['id']?>" />
							<div class="form-group">	
								<label class="sr-only" for="name"></label>
								<input type="text" name="name" id="name" value="<?=$row_fid['name']?>" class="form-control"/>
							</div>
							<div class="form-group">	
								<label class="sr-only" for="update_class"></label>
								<input type="submit" name="update_class" id="update_class" value="更 新" class="btn btn-default" />
							</div>
							<div class="form-group">	
								<label class="sr-only" for="del"></label>
								<input type="button" value="删 除" id="del" onclick="location.href='?del=<?=$row_fid['id']?>'"  class="btn btn-default" />
							</div>
						</form>
						<?php
						}
					?>
				</td>
			</tr>
			<?php
			}
		?>
	</table>
</div>
</div>
<!--panel end-->
</div>
</body>
</html>