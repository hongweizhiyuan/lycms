<?php
	include_once 'admin_global.php';
	$r = $db->Get_user_shell_check($uid, $shell);
	if(isset($_POST['submit_news'])){
		$db->query("INSERT INTO `n_newsbase`(`cid`,`title`,`author`,`datetime`) VALUES('$_POST[cid]','$_POST[title]','$_POST[author]','".mktime()."')");
		$last_id = $db->insert_id();
		$db->query("INSERT INTO `n_newscontent`(`nid`,`keyword`,`content`,`remark`) VALUES('$last_id','$_POST[keyword]','$_POST[content]','')");
		$db->Get_admin_msg("admin_news_add.php","新闻添加成功");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>后台管理-添加新闻</title>
		<meta http-equiv=Content-Type content="text/html; charset=gb2312">
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/jquery-1.9.0.min.js"></script>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet">
		<script src="/public/js/bootstrap.min.js"></script>
		<link href="/public/ueditor-min/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="/public/ueditor-min/third-party/jquery.min.js"></script>
		<script type="text/javascript" charset="gbk" src="/public/ueditor-min/umeditor.config.js"></script>
		<script type="text/javascript" charset="gbk" src="/public/ueditor-min/umeditor.min.js"></script>
		<script type="text/javascript" src="/public/ueditor-min/lang/zh-cn/zh-cn.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- 菜单 -->
		<?php require_once "admin_menu.php"; ?>
		
		<!-- 主体 -->
		<div class="table-responsive w98b">   
			<div class="panel panel-default">
				<div class="panel-heading">
					后台管理 &gt;&gt; 添加新闻
				</div>
				<div class="panel-body">
					<form action="" method="post"  class="form-horizontal" >
						
						<div class="form-group">
							<label for="cid" class="col-sm-2 control-label">选择分类：</label>
							<div class="col-sm-9">
								<select name="cid" id="cid"  class="form-control ">
									<?php
										$result = mysql_query("SELECT * FROM `n_newsclass` WHERE `f_id`=0");
										while($row = mysql_fetch_array($result)){
											echo "<option value='$row[id]'>".$row['name']."</option>";
											$class_result = mysql_query("SELECT * FROM `n_newsclass` WHERE `f_id`='$row[id]'");
											while($class_row = mysql_fetch_array($class_result)){
												echo "<option value='$class_row[id]'>&nbsp;┗".$class_row['name']."</option>";
											} 
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">新闻标题：</label>
							<div class="col-sm-9">
								<input type="text" name="title" id="title" size="40" class="form-control "/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="author" class="col-sm-2 control-label">新闻作者：</label>
							<div class="col-sm-9">
								<input type="text" name="author" id="author" class="form-control " />
							</div>
						</div>
						
						<div class="form-group">
							<label for="keyword" class="col-sm-2 control-label">新闻关键字：</label>
							<div class="col-sm-9">
								<input type="text" name="keyword" id="keyword" size="70"  class="form-control "/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="myEditor" class="col-sm-2 control-label">新闻内容：</label>
							<div class="col-sm-9">
								<textarea name="content" id="myEditor" cols="80" rows="8"  style="width:100%;height:240px;"></textarea>
								<script type="text/javascript">var um = UM.getEditor('myEditor');</script>
							</div>
						</div>
						
						<div class="form-group text-center">
							<input type="submit" name="submit_news" value="添 加" class="btn btn-large btn-primary"/>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</body>
</html>			