<?php
	include_once ('admin_global.php');
	
	$r=$db->Get_user_shell_check($uid, $shell); //判断是否登陆
	
	if($_GET[action]=='logout')$db->Get_user_out();//如果为真 那么退出
	
	$query=$db->findall("n_config");//执行select语句
	while($row=$db->fetch_array($query)){
		$row_arr[$row[name]]=$row[values];//得到的值付给一个变量
	}
	
	if(isset($_POST['update'])){ //判断update函数是否存在
		unset($_POST['update']); //删除函数
		foreach($_POST as $name=>$values){ //执行插入语句
			$db->query("update n_config set `values`='$values' where `name`='$name'");//同上的$row[values]
		}
		$db->Get_admin_msg("admin_main.php");//成功后跳转
	}
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>后台管理-系统配置</title>
		<meta http-equiv=content-type content="text/html; charset=gb2312">
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/jquery-1.9.0.min.js"></script>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet">
		<script src="/public/js/bootstrap.min.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- 菜单 -->
		<?php require_once "admin_menu.php"; ?>
		
		<!-- 主体 -->
		<div class="table-responsive w98b">   
			<div class="panel panel-default">
				<div class="panel-heading">
					后台管理 &gt;&gt; 后台首页
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<tr>
							<th colspan="4" class="group-title active">系统信息</th>
						</tr>
						<tr>
							<td width="20%">服务器操作系统:</td>
							<td width="30%"><?php echo PHP_OS; ?></td>
							<td width="20%">Web 服务器:</td>
							<td width="30%"><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
						</tr>
						<tr>
							<td>PHP 版本:</td>
							<td><?php echo PHP_VERSION; ?></td>
							<td>MySQL 版本:</td>
							<td><?php echo $db->mysql_server(); ?></td>
						</tr>
						<tr>
							<td>Socket 支持:</td>
							<td><?php echo function_exists('fsockopen') ? "是" : "否"; ?></td>
							<td>时区设置:</td>
							<td><?php echo function_exists("date_default_timezone_get") ? date_default_timezone_get() : "没有时区"; ?></td>
						</tr>
						<tr>
							<td>Zlib 支持:</td>
							<td><?php echo function_exists('gzclose') ? "是" :"否"; ?></td>
							<td>文件上传的最大大小:</td>
							<td><?php echo ini_get('upload_max_filesize'); ?></td>
						</tr>
						<tr>
							<td>编码:</td>
							<td>GBK</td>
							<td>老杨CMS版本:</td>
							<td> v1.01</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		
	</body>
</html>
