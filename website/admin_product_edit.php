<?php
  include_once 'admin_global.php';
  $r = $db->Get_user_shell_check($uid, $shell);
  if(isset($_POST['update_product'])){
  	//上传图片
	$img = new fileUp();
	$img->inputName = "litpic";
	$img->up();
	$imgname = $img->destination;
	//插入数据库	
  	$time = mktime();
  	$sql="UPDATE `n_productbase` SET `cid`='$_POST[cid]',`title`='$_POST[title]',`author`='$_POST[author]',";
  	//是否有缩略图
  	if(!empty($imgname)){
  		$sql.="`litpic`='$imgname',";
  	}
  	$sql.="`datetime`='$time' WHERE `id`='$_GET[id]'";
  	$db->query($sql);
  	$db->query("UPDATE `n_productcontent` SET `keyword`='$_POST[keyword]',`content`='$_POST[content]',`sessionid`='$_POST[sessionid]' WHERE `nid`='$_GET[id]'");
  	$db->Get_admin_msg("admin_product_list.php","成功修改作品");
  }
  if(!empty($_GET['id'])){
  	$res = $db->query("SELECT * FROM `n_productbase` b, `n_productcontent` c WHERE b.`id`=c.`nid` and b.id='$_GET[id]'");
  	$row_product = $db->fetch_array();  	
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>后台管理-编辑作品</title>
		<meta http-equiv=Content-Type content="text/html; charset=gb2312">
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/jquery-1.9.0.min.js"></script>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet">
		<script src="/public/js/bootstrap.min.js"></script>
		<link href="/public/ueditor-min/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="/public/ueditor-min/third-party/jquery.min.js"></script>
		<script type="text/javascript" charset="gbk" src="/public/ueditor-min/umeditor.config.js"></script>
		<script type="text/javascript" charset="gbk" src="/public/ueditor-min/umeditor.min.js"></script>
		<script type="text/javascript" src="/public/ueditor-min/lang/zh-cn/zh-cn.js"></script>
		<link href="/public/js/swfupload/swfupload.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="/public/js/swfupload/swfupload.js"></script>
		<script type="text/javascript" src="/public/js/swfupload/swfupload.queue.js"></script>
		<script type="text/javascript" src="/public/js/swfupload/fileprogress.js"></script>
		<script type="text/javascript" src="/public/js/swfupload/handlers.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		var swfu;

		window.onload = function() {
			var settings = {
				flash_url : "/public/js/swfupload/swfupload.swf",
				upload_url: "/website/admin_uploadpic.php",	
				post_params: {"PHPSESSID" : "<?php echo $row_product['sessionid']; ?>"},
				file_size_limit : "100 MB",
				file_types : "*.*",
				file_types_description : "All Files",
				file_upload_limit : 10,  //配置上传个数
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings
				button_image_url: "/public/js/swfupload/TestImageNoText_65x29.png",
				button_width: "65",
				button_height: "29",
				button_placeholder_id: "spanButtonPlaceHolder",
				button_text: '<span class="theFont">浏览</span>',
				button_text_style: ".theFont { font-size: 16; }",
				button_text_left_padding: 12,
				button_text_top_padding: 3,
				
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	
			};

			swfu = new SWFUpload(settings);
	     };
	</script>
	</head>
	<body>
		<!-- 菜单 -->
		<?php require_once "admin_menu.php"; ?>
		
		<!-- 主体 -->
		<div class="table-responsive w98b">   
			<div class="panel panel-default">
				<div class="panel-heading">
					后台管理 &gt;&gt; 编辑作品
				</div>
				<div class="panel-body">
					<form action="" method="post"  class="form-horizontal" enctype="multipart/form-data">
						<div class="form-group">
							<label for="cid" class="col-sm-2 control-label">选择作品分类：</label>
							<div class="col-sm-9">
								<select name="cid" id="cid"  class="form-control ">
									<?php
							          $result = mysql_query("SELECT * FROM `n_productclass` WHERE `f_id`=0");
							          while($row = mysql_fetch_array($result)){
							          	$selected = $row['id']==$row_product['cid'] ? "selected" : NULL;
							          	echo "<option value='$row[id]' $selected>".$row['name']."</option>";
							              $class_result = mysql_query("SELECT * FROM `n_productclass` WHERE `f_id`='$row[id]'");
							          	  while($class_row = mysql_fetch_array($class_result)){
							          	  	$selected = $class_row['id']==$row_product['cid'] ? "selected" : NULL;
							          	  	echo "<option value='$class_row[id]' $selected>&nbsp;┗".$class_row['name']."</option>";
							          	  } 
							          }
							        ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">作品标题：</label>
							<div class="col-sm-9">
								<input type="text" name="title" id="title" size="40" class="form-control" value="<?php echo $row_product['title']; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label for="author" class="col-sm-2 control-label">作品作者：</label>
							<div class="col-sm-9">
								<input type="text" name="author" id="author" class="form-control " value="<?php echo $row_product['author']; ?>"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="litpic" class="col-sm-2 control-label">产品缩略图：</label>
							<div class="col-sm-4">
								<img src="<?php echo $row_product['litpic']; ?>" width="120" height="80" border="0" style='padding:5px;'/>
							</div>
							<div class="col-sm-5">
								<input type="file" name="litpic" id="litpic" value="<?php echo $row_product['litpic']; ?>"  />
							</div>
						</div>
						
						<div class="form-group">
							<label for="pictures" class="col-sm-2 control-label">产品列表：</label>
							<div class="col-sm-4">
							        <?php 
							$sql="select * from n_uploadpic where sessionid='$row_product[sessionid]'"; 
							$query = $db->query($sql);
							
							while ($resultpic = $db->fetch_array($query)){
								echo "<img src='/uploads/product/$resultpic[pictures]' width='120' height='80' border='0' style='padding:5px;'/>";
							}
							?>
							</div>
							<div class="col-sm-5">
								<div class="fieldset flash" id="fsUploadProgress">
									<span class="legend">快速上传</span>
			  					</div>
								<div id="divStatus">0 个文件已上传</div>
								<div>
									<span id="spanButtonPlaceHolder"></span>
									<input id="btnCancel" type="button" value="取消所有上传" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
									<input type="hidden" value="<?php echo $row_product['sessionid']; ?>" name="sessionid" id="sessionid" />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="keyword" class="col-sm-2 control-label">作品关键字：</label>
							<div class="col-sm-9">
								<input type="text" name="keyword" id="keyword" size="70"  class="form-control" value="<?php echo $row_product['keyword']; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label for="myEditor" class="col-sm-2 control-label">作品内容：</label>
							<div class="col-sm-9">
								<textarea name="content" id="myEditor" cols="80" rows="8"  style="width:100%;height:240px;"><?php echo $row_product['content']; ?></textarea>
								<script type="text/javascript">var um = UM.getEditor('myEditor');</script>
							</div>
						</div>
						<div class="form-group text-center">
							<input type="submit" name=update_product value="更新" class="btn btn-large btn-primary"/>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</body>
</html>