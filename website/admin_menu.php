<nav class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="admin_main.php"><strong>老杨CMS管理系统</strong></a>
	</div>
	<div>
		<ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					系统管理 
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="admin_main.php">后台首页</a></li>
					<li><a href="admin_config.php">配置信息</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					新闻管理 
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="admin_news_list.php">新闻列表</a></li>
					<li><a href="admin_news_add.php">添加新闻</a></li>
                    <li><a href="admin_news_class.php">新闻分类</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					作品管理 
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="admin_product_list.php">作品列表</a></li>
					<li><a href="admin_product_add.php">添加作品</a></li>
                    <li><a href="admin_product_class.php">作品分类</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					单页管理 
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="admin_aboutus.php">关于我们</a></li>
					<li><a href="admin_contact.php">联系方式</a></li>
				</ul>
			</li>
			
			<li class=""><a href="../index.php" target="_blank">前台预览</a></li>
			<li><a onClick="return confirm('提示：您确定要退出系统吗？')" href="admin_main.php?action=logout">退出后台</a></li>
		</ul>
	</div>
</nav>