<?php
	include_once ('admin_global.php');
	
	$r=$db->Get_user_shell_check($uid, $shell); //判断是否登陆
	
	if($_GET[action]=='logout')$db->Get_user_out();//如果为真 那么退出

	$sql ="select content from n_pages where name='联系方式'";
	$query = $db->query($sql);
	$row = $db->fetch_array($query);
	
	 if(isset($_POST['update'])){
		$sql2 ="update n_pages set `content`='$_POST[content]' where `name`='联系方式'";
		$db->query($sql2);
		$db->Get_admin_msg("admin_contact.php");//成功后跳转
	}
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>后台管理-系统配置</title>
		<meta http-equiv=content-type content="text/html; charset=gb2312">
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/jquery-1.9.0.min.js"></script>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet">
		<script src="/public/js/bootstrap.min.js"></script>
		<link href="/public/ueditor-min/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="/public/ueditor-min/third-party/jquery.min.js"></script>
		<script type="text/javascript" charset="gbk" src="/public/ueditor-min/umeditor.config.js"></script>
		<script type="text/javascript" charset="gbk" src="/public/ueditor-min/umeditor.min.js"></script>
		<script type="text/javascript" src="/public/ueditor-min/lang/zh-cn/zh-cn.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- 菜单 -->
		<?php require_once "admin_menu.php"; ?>
		
		<!-- 主体 -->
		<div class="table-responsive w98b"> 
			<div class="panel panel-default">
				<div class="panel-heading">
					后台管理 &gt;&gt; 联系方式
				</div>
				<div class="panel-body">
					<form action="" method="post" >
						<div class="col-sm-12">
							<textarea name="content" id="contact" cols="80" rows="8"  style="width:100%;height:240px;"><?php echo $row[content]?></textarea>
							<script type="text/javascript">var um = UM.getEditor('contact');</script>
						</div>
						
						<div class="form-group text-center ">
							<input type="submit" name="update" value="更 新" class="btn btn-large btn-primary mt10"/>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</body>
</html>
