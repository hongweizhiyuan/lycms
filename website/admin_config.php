<?php
	include_once ('admin_global.php');
	
	$r=$db->Get_user_shell_check($uid, $shell); //判断是否登陆
	
	if($_GET[action]=='logout')$db->Get_user_out();//如果为真 那么退出
	
	$query=$db->findall("n_config");//执行select语句
	while($row=$db->fetch_array($query)){
		$row_arr[$row[name]]=$row[values];//得到的值付给一个变量
	}
	
	if(isset($_POST['update'])){ //判断update函数是否存在
		unset($_POST['update']); //删除函数
		foreach($_POST as $name=>$values){ //执行插入语句
			$db->query("update n_config set `values`='$values' where `name`='$name'");//同上的$row[values]
		}
		$db->Get_admin_msg("admin_config.php");//成功后跳转
	}
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>后台管理-系统配置</title>
		<meta http-equiv=content-type content="text/html; charset=gb2312">
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/jquery-1.9.0.min.js"></script>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet">
		<script src="/public/js/bootstrap.min.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- 菜单 -->
		<?php require_once "admin_menu.php"; ?>
		
		<!-- 主体 -->
		<div class="table-responsive w98b">   
			<div class="panel panel-default">
				<div class="panel-heading">
					后台管理 &gt;&gt; 系统配置
				</div>
				<div class="panel-body">
					<form action="" method="post" class="form-horizontal" >
						<div class="form-group">
							<label for="website_name" class="col-sm-2 control-label">网站名称：</label>
							<div class="col-sm-9">
								<input type="text" name="website_name" id="website_name" value="<?php echo $row_arr[website_name]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label for="website_url" class="col-sm-2 control-label">网站地址：</label>
							<div class="col-sm-9">
								<input type="text" name="website_url" id="website_url" value="<?php echo $row_arr[website_url]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label for="website_keyword" class="col-sm-2 control-label">关键字：</label>
							<div class="col-sm-9">
								<input type="text" name="website_keyword" id="website_keyword" value="<?php echo $row_arr[website_keyword]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label for="website_description" class="col-sm-2 control-label">简介：</label>
							<div class="col-sm-9">
								<input type="text" name="website_description" id="website_description" value="<?php echo $row_arr[website_description]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label for="website_tel" class="col-sm-2 control-label">电话：</label>
							<div class="col-sm-9">
								<input type="text" name="website_tel" id="website_tel" value="<?php echo $row_arr[website_tel]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label for="website_tel" class="col-sm-2 control-label">email：</label>
							<div class="col-sm-9">
								<input type="text" name="website_tel" id="website_tel" value="<?php echo $row_arr[website_tel]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label for="website_qq" class="col-sm-2 control-label">QQ号码：</label>
							<div class="col-sm-9">
								<input type="text" name="website_qq" id="website_qq" value="<?php echo $row_arr[website_qq]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label for="website_weibo" class="col-sm-2 control-label">新浪微博：</label>
							<div class="col-sm-9">
								<input type="text" name="website_weibo" id="website_weibo" value="<?php echo $row_arr[website_weibo]?>"  class="form-control "/>
							</div>
						</div>
						<div class="form-group text-center" >
							<input type="submit" class="btn btn-default" name="update" value="更 新" />
						</div>
					</form>
				</div>
			</div>
		</div>

		
	</body>
</html>
