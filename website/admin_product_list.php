<?php
	include_once 'admin_global.php';
	$r = $db->Get_user_shell_check($uid, $shell);
	$query = $db->findall("n_productclass");
	while($row=$db->fetch_array()){
		$product_class_arr[$row['id']] = $row['name'];
	}
	if(isset($_GET['del'])){
		mysql_query("DELETE FROM `n_productbase` WHERE `id` = '$_GET[del]'");
		mysql_query("DELETE FROM `n_productcontent` WHERE `nid` = '$_GET[del]'");
		$db->Get_admin_msg("admin_product_list.php","删除成功作品");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>后台管理-作品列表</title>
		<meta http-equiv=content-type content="text/html; charset=gb2312">
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/jquery-1.9.0.min.js"></script>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet">
		<script src="/public/js/bootstrap.min.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- 菜单 -->
		<?php require_once "admin_menu.php"; ?>
		
		<!-- 主体 -->
		<div class="table-responsive w98b">  
			<div class="panel panel-default">
				<div class="panel-heading">
					后台管理 &gt;&gt; 作品列表
				</div>
				<div class="panel-body">
					<form action="" method="post" >
						<table class="table table-striped table-bordered">
							<thead>
								<th width="50">ID</th>
								<th width="">作品标题</th>
								<th width="100">作品分类</th>
								<th width="100">作者</th>
								<th width="200">日期</th>
								<th width="100">操作</th>
							</thead>
							<tbody>
								<?php
									$result = $db->query("SELECT `ID` FROM `n_productbase` order by id desc");
									$total = $db->db_num_rows();
									pageft($total, 20);
									if($firstcount < 0){
										$firstcount = 0;
									}
									$query = $db->findall("n_productbase order by id desc limit  $firstcount, $displaypg");
									while($row = $db->fetch_array()){
								?>
								<tr>
									<td><?php echo $row['id']; ?></td>
									<td><?php echo $row['title']; ?></td>
									<td><?php echo $product_class_arr[$row['cid']]; ?></td>
									<td><?php echo $row['author']; ?></td>
									<td><?php echo date("Y-m-d H:i",$row['datetime']); ?></td>
									<td><a href='?del=<?php echo $row[id]; ?>'>删除</a> / <a href='admin_product_edit.php?id=<?php echo $row[id]; ?>'>修改</a></td>
								</tr>
								<?php
									}
								?>
								<tr>
									<th colspan="5"><?=$pagenav;?></th>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>									