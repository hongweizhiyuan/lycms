<?php
    include_once('admin_global.php');
    //如果用户名和密码都不为空 即条件为假 执行$db进行判断
    if(!empty($_POST[username])&& !empty($_POST[password])) $db->Get_user_login($_POST[username],$_POST[password]);
?>
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="gb2312"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>老杨CMS管理系统</title>
		<link href="/public/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="/public/css/common.css" rel="stylesheet"/>
		<script src="/public/js/require.js"></script>
		<script src="/public/js/config.js"></script>
		<!--[if lt IE 9]>
			<script src="/public/js/html5shiv.min.js"></script>
			<script src="/public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="login">
			<div class="logo">
				<h2>老杨CMS管理系统</h2>
			</div>
			<div class="clearfix" style="margin-bottom:5em;">
				<div class="panel panel-default container" style="width:50%;">
					<div class="panel-body">
						<form action="" method="post" role="form">
							<div class="form-group input-group">
								<div class="input-group-addon"><i class="fa fa-user"></i></div>
								<input name="username" type="text" class="form-control input-lg" placeholder="请输入用户名登录">
							</div>
							<div class="form-group input-group">
								<div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
								<input name="password" type="password" class="form-control input-lg" placeholder="请输入登录密码">
							</div>
							<div class="form-group">
								<label class="checkbox-inline input-lg">
									<input type="checkbox" value="true" name="rememberme"> 记住用户名 
								</label>
								<div class="pull-right">
									<input type="submit" name="submit" value="登录" class="btn btn-primary btn-lg" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="center-block footer" role="footer">
				<div class="text-center">
					Powered by <a href="http://www.360sites.cn"><b>老杨</b></a> v0.10 &copy; 2014 <a href="http://www.360sites.cn">www.360sites.cn</a>		
				</div>
			</div>
		</div>
		<script>
		require(['jquery'],function($){
			var h = document.documentElement.clientHeight;
			$(".login").css('min-height',h);
		});
		</script>
	</body>
</html>







