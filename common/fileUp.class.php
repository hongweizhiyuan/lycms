<?php
/*----------------------------------------------------------------------------------
 * 图片上传类，功能包括：
 * 1.上传图片
 * 2.生成略缩图
 * 3.生成预览图
 * 注释：
 * 1).每个值都设定了初始值，可以不配置
 * 2).文件的每个信息都可通过外部获取
 * 命名规则：
 * (1).变量：两个单词使用大写字母区分，超过两个单词，则后面的单词使用下划线分开
 * (2).方法：单词之间使用下划线分开
 *---------------------------------------------------------------------------------- 
 */
class fileUp{
	//定义基本参数
	private $upType             =  array('image/jpg','image/jpeg','image/png','image/pjpeg','image/gif','image/bmp','image/x-png');  //上传文件类型
	private $maxFileSize        =  1024000;        //上传大小限制(单位:B)
	private $upFolder           =  "../uploads/";         //上传文件路径
	private $waterMark          =  0;             //是否附加水印(1为附加，0为不附加)
	private $waterType          =  1;             //水印类型(1为文字,2为图片)
	private $waterPosition      =  1;             //水印位置(1为左下角,2为右下角,3为左上角,4为右上角,5为居中);
	private $waterString        =  null;          //水印字符串
	private $waterImg           =  null;          //水印图片
	private $imgPreview         =  1;             //是否生成预览图(1为生成,其他为不生成);
	private $imgPreview_size    =  0;             //预览图比例，0为按固定宽或高显示，其他为比例显示
	private $imgPreview_width   =  300;           //预览图固定宽度
	private $imgPreview_height  =  300;           //预览图固定高度
	//+++++++++++++++++++++++++++++++++++++++++
	private $Thumbnail          =  0;             //是否生成且保存略缩图,1为生成，0为不生成
	private $Thumbnail_folder   =  null;          //略缩图保存路径,默认与文件路径一致
	private $Thumbnail_fixed    =  0;             //略缩图是否使用固定宽高，1为使用，0为灵活变动
	private $Thumbnail_width    =  200;           //略缩图宽度
	private $Thumbnail_height   =  200;           //略缩图高度
	
	private $Thumbnail_name     =  null;          //略缩图名称
	//******************************************************************************************************************
	private $inputName          =  "litpic";      //文件上传框名称
	//******************************************************************************************************************
	private $imgPreview_display =  null;          //图片预览图显示
	//******************************************************************************************************************
	//文件上传相关信息，1为文件不存在，2为类型不符合，3为超出大小限制，4为上传失败，0为上传成功
	private $fileUp_info        =  null;
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//可在外部获取上传文件基本信息
	private $fileName_or;                         //客服端文件的原名称
	private $fileType;                            //文件的MIME类型
	private $fileSize;                            //已上传文件的大小，单位/字节
	private $fileTmp_name;                        //储存的临时文件名
	private $fileUp_error;                        //该文件上传相关错误代码

	private $imgSize;                             //取得图片的长宽
	private $fileBasename;                        //获取带扩展名的全名
	private $fileExtension;                       //获取文件扩展名
	private $fileName;                            //文件名（不带扩展名)
	private $destination;                         //文件路径加名称
	//******************************************************************************************************************
	public function __set($propety_name,$value){
		$this->$propety_name=$value;
	}
	public function __get($property_name){
		if(isset($this->$property_name))
		return($this->$property_name);
		else return(NULL);
	}
	//******************************************************************************************************************
	//定义文件上传功能
	public function up(){
		//判断文件是否存在
		if(!is_uploaded_file($_FILES[$this->inputName]["tmp_name"])){
			$this->fileUp_info=1;
			return;
		}
		//获取并赋值相应基本参数
		$upfile=$_FILES[$this->inputName];
		$this->fileName_or=$upfile["name"];
		$this->fileType=$upfile["type"];
		$this->fileSize=$upfile["size"];
		$this->fileTmp_name=$upfile["tmp_name"];
		$this->fileUp_error=$upfile["error"];
		//检查文件类型是否符合
		if(!in_array($this->fileType,$this->upType)){
			$this->fileUp_info=2;
			return;
		}
		//检查文件大小是否超出限制
		if($this->fileSize>$this->maxFileSize){
			$this->fileUp_info=3;
			return;
		}
		//判断目录是否存在
		if(!file_exists($this->upFolder))
		mkdir($this->upFolder);
		//进一步取得图片的信息并赋值
		$this->imgSize          =  getimagesize($this->fileTmp_name);
		$pathinfo               =  pathinfo($this->fileName_or);
		$this->fileExtension    =  $pathinfo["extension"];          //获取文件扩展名
		$this->fileBasename     =  $pathinfo["basename"];           //获取带扩展名的全名
		$this->fileName         =  $pathinfo["filename"];           //文件名（不带扩展名)
		$fileName2              =  $pathinfo['filename'];
		$this->destination      =  $this->upFolder.$this->fileName.".".$this->fileExtension;
		//判断文件名是否存在，如果存在则重命名
		$n=1;
		while (file_exists($this->destination)){
			while (file_exists($this->destination)){
				$n++;
				$this->fileName=$this->fileName."(".$n.")";
				$this->destination = $this->upFolder.$this->fileName.".".$this->fileExtension;
			}
			$this->fileName=$fileName2."(".$n.")";
			$this->destination = $this->upFolder.$this->fileName.".".$this->fileExtension;
		}
		//移动上传的文件
		if(move_uploaded_file($this->fileTmp_name,$this->destination))
		$this->fileUp_info=0;
		else $this->fileUp_info=4;
			
		//生成略缩图
		if($this->Thumbnail==1){
			$this->Thumbnail();
		}
		//添加水印
		if($this->waterMark==1){
			$this->add_waterMark();
		}
		//生成预览图
		if($this->imgPreview_size == 0){
			
			$imgPreview_height=($this->imgSize[1]/$this->imgSize[0])*$this->imgPreview_width;
			if($imgPreview_height>$this->imgPreview_height){
				$this->imgPreview_width = ($this->imgSize[0]/$this->imgSize[1])*$this->imgPreview_height;
			}
		    $imgPreview_width=($this->imgSize[0]/$this->imgSize[1])*$this->imgPreview_height;
			if($imgPreview_width>$this->imgPreview_width){
				$this->imgPreview_height=($this->imgSize[1]/$this->imgSize[0])*$this->imgPreview_width;
			}
			
			if($this->imgSize["0"]<$this->imgPreview_width){
				$this->imgPreview_width=$this->imgSize["0"];
				$this->imgPreview_height=($this->imgSize[1]/$this->imgSize[0])*$this->imgPreview_width;
			} 
			if($this->imgSize["1"]<$this->imgPreview_height){
				$this->imgPreview_height=$this->imgSize["1"];
				$this->imgPreview_width = ($this->imgSize[0]/$this->imgSize[1])*$this->imgPreview_height;
			}
		}else{
			$this->imgPreview_width=$this->imgSize["0"]*$this->imgPreview_size;
			$this->imgPreview_height=$this->imgSize["1"]*$this->imgPreview_size;
		}
		$this->imgPreview_display="<img src='$this->destination' width='$this->imgPreview_width' height='$this->imgPreview_height'
                                    alt='图片预览：\r文件名'：$this->fileTmp_name />";
	}
	//====================================================================================================================
	//上传功能结束，子功能开始
	//====================================================================================================================	
	//生成略缩图功能
	function Thumbnail(){
		if($this->Thumbnail_folder==null)
		  $this->Thumbnail_folder=$this->upFolder;
		
		//$this->Thumbnail_name=$this->fileName."_t.".$this->fileExtension;
		$Thumbnail_name_b=$this->fileName."_thu";
		$Thumbnail_name_b2=$this->fileName."_thu";
		$destination_b=$this->Thumbnail_folder.$Thumbnail_name_b.".".$this->fileExtension;
	    //判断文件名是否存在，如果存在则重命名
		$n=1;
		while (file_exists($destination_b)){
			while (file_exists($destination_b)){
				$n++;
				$Thumbnail_name_b=$Thumbnail_name_b."(".$n.")";
				$destination_b = $this->Thumbnail_folder.$Thumbnail_name_b.".".$this->fileExtension;
			}
			$Thumbnail_name_b=$Thumbnail_name_b2."(".$n.")";
			$destination_b = $this->Thumbnail_folder.$Thumbnail_name_b.".".$this->fileExtension;
		}
		
		
		$imgInfo=getimagesize($this->destination);
		switch($imgInfo[2])
		{
			case 1:
				$in=@imagecreatefromgif($this->destination);
				break;
			case 2:
				$in=@imagecreatefromjpeg($this->destination);
				break;
			case 3:
				$in=@imagecreatefrompng($this->destination);
				break;
			case 6:
				$in =@imagecreatefrombmp($this->destination);
				break;
			default:
				break;
		}
		//计算略缩图长宽
		if($this->Thumbnail_fixed==0){
			if($this->Thumbnail_height>($imgInfo[1]/$imgInfo[0])*$this->Thumbnail_width)                            
				$this->Thumbnail_width = ($imgInfo[0]/$imgInfo[1])*$this->Thumbnail_height;
			else $this->Thumbnail_height=($imgInfo[1]/$imgInfo[0])*$this->Thumbnail_width;
		}
		if($this->Thumbnail_width > $imgInfo[0]){
			$this->Thumbnail_width = $imgInfo[0];
			$this->Thumbnail_height=($imgInfo[1]/$imgInfo[0])*$this->Thumbnail_width;
		}
		if($this->Thumbnail_height > $imgInfo[1]){
			$this->Thumbnail_height = $imgInfo[1];
			$this->Thumbnail_width = ($imgInfo[0]/$imgInfo[1])*$this->Thumbnail_height;
		}
		
		$new = imageCreateTrueColor($this->Thumbnail_width,$this->Thumbnail_height);
		ImageCopyResized($new,$in,0,0,0,0,$this->Thumbnail_width,$this->Thumbnail_height,$imgInfo[0],$imgInfo[1]);
		switch ($imgInfo[2])
		{
			case 1:
				imagejpeg($new,$destination_b);
				break;
			case 2:
				imagejpeg($new,$destination_b);
				break;
			case 3:
				imagepng($new,$destination_b);
				break;
			case 6:
				imagewbmp($new,$destination_b);
				break;
		}
	}
	//====================================================================================================================
	//====================================================================================================================	
	//添加水印功能
	function add_waterMark(){
		//1 = GIF，2 = JPG，3 = PNG，4 = SWF，5 = PSD，6 = BMP，7 = TIFF(intel byte order)，
		//8 = TIFF(motorola byte order)，9 = JPC，10 = JP2，11 = JPX，12 = JB2，13 = SWC，14 = IFF，15 = WBMP，16 = XBM。
		$imgInfo=getimagesize($this->destination);
		$im=imagecreatetruecolor($this->imgSize[0],$this->imgSize[1]);         //创建真彩色
		$white=imagecolorallocate($im,255,255,255);                            //设置颜色
		$black=imagecolorallocate($im,0,0,0);
		$red=imagecolorallocate($im,255,0,0);
		//在 image 图像的坐标 x，y（图像左上角为 0, 0）处用 color 颜色执行区域填充（即与 x, y 点颜色相同且相邻的点都会被填充）。
		imagefill($im,0,0,$white);

		switch($imgInfo[2])
		{
			//case 1:
				//$simage =imagecreatefromgif($this->destination);              // 创建一个新的形象，从文件或 URL
				//break;
			case 2:
				$simage =imagecreatefromjpeg($this->destination);
				break;
			case 3:
				$simage =imagecreatefrompng($this->destination);
				break;
			case 6:
				$simage =imagecreatefromwbmp($this->destination);
				break;
			default:
				//echo ("不支持的文件类型");
				break;
		}
		if(!empty($simage))
		{
			//位置设置
			if($this->waterType==1){
				$str_len=strlen($this->waterString);
			    $str_width=$str_len*10;
			    $str_height=20;
			}elseif($this->waterType==1 && file_exists($this->waterImg)){
				$iinfo=getimagesize($this->waterImg);
				$str_width = $iinfo[0];
				$str_height = $iinfo[1];
			}
			
			switch ($this->waterPosition){
				case 1:
					$p_x=5;
					$p_y=$this->imgSize[1]-$str_height;
					break;
				case 2:
					$p_x=$this->imgSize[0]-$str_width;
					$p_y=$this->imgSize[1]-$str_height;
					break;
				case 3:
					$p_x=5;
					$p_y=0;
					break;
				case 4:
					$p_x=$this->imgSize[0]-$str_width;
					$p_y=5;
					break;
				case 5:
					$p_x=($this->imgSize[0]-$str_width)/2;
					$p_y=($this->imgSize[1]-$str_height)/2;
					break;
			}
			imagecopy($im,$simage,0,0,0,0,$this->imgSize[0],$this->imgSize[1]);   //拷贝图像的一部分
			//imagefilledrectangle($im,1,$this->imgSize[1]-15,130,$this->imgSize[1],$white);  //将图片的封闭长方形区域着色

			switch($this->waterType)
			{
				case 1:   //加水印字符串
					imagestring($im,10,$p_x,$p_y,$this->waterString,$red);
					break;
				case 2:   //加水印图片
					$simage1 =imagecreatefromgif($this->waterImg);
					imagecopy($im,$simage1,0,0,0,0,85,15);
					imagedestroy($simage1);
					break;
			}

			switch ($imgInfo[2])
			{
				case 1:
					//imagegif($nimage, $destination);
					imagejpeg($im, $this->destination);
					break;
				case 2:
					imagejpeg($im, $this->destination);
					break;
				case 3:
					imagepng($im, $this->destination);
					break;
				case 6:
					imagewbmp($im, $this->destination);
					break;
			}
			//覆盖原上传文件
			imagedestroy($im);
			imagedestroy($simage);
		}
	}
}
?>


















