<?php
class action extends mysql {

	/**
	 * 用户权限判断($uid, $shell, $m_id)
	 */

	public function Get_user_shell($uid, $shell) {
		$query = $this->select('n_admin', '*', '`uid` = \'' . $uid . '\'');
		$us = is_array($row = $this->fetch_array($query));
		$shell = $us ? $shell == md5($row[username] . $row[password] . "TKBK") : FALSE;
		return $shell ? $row : NULL;
	} //end shell

	public function Get_user_shell_check($uid, $shell, $m_id = 9) {
		if ($row=$this->Get_user_shell($uid, $shell)) {
			if ($row[m_id] <= $m_id) {
				return $row;
			} else {
				echo "你无权限操作！";
				exit ();
			} //end m_id
		} else {
		  $this->Get_admin_msg('index.php','请先登陆');
		}
	} //end shell
	//========================================


	/**
	 * 用户登陆超时时间(秒)
	 */
	public function Get_user_ontime($long = '3600') {
		$new_time = mktime();
		$onlinetime = $_SESSION[ontime];
		echo $new_time - $onlinetime;
		if ($new_time - $onlinetime > $long) {
			echo "登录超时";
			session_destroy();
			exit ();
		} else {
			$_SESSION[ontime] = mktime();
		}
	}

	/**
	 * 用户登陆
	 */
	public function Get_user_login($username, $password) {
		$username = str_replace(" ", "", $username);//去掉空格
		$query = $this->select('n_admin', '*', '`username` = \'' . $username . '\'');
		$us = is_array($row = $this->fetch_array($query));
		$ps = $us ? md5($password) == $row[password] : FALSE;
		if ($ps) {
			$_SESSION[uid] = $row[uid];
			$_SESSION[shell] = md5($row[username] . $row[password] . "TKBK");
			$_SESSION[ontime] = mktime();
			$this->Get_admin_msg('admin_main.php','登陆成功！');
		} else {
			$this->Get_admin_msg('index.php','密码或用户错误！');
			session_destroy();
		}
	}
	 /**
	  * 用户退出登陆
	  */
	public function Get_user_out() {
		session_destroy();
		$this->Get_admin_msg('index.php','退出成功！');
	}

   /**
    * 后台通用信息提示
    */
	public function Get_admin_msg($url, $show = '操作已成功！') {
		$msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
        				<link href="/public/css/bootstrap.min.css" rel="stylesheet"/>
                        <link href="/public/css/font-awesome.min.css" rel="stylesheet"/>
                        <link href="/public/css/common.css" rel="stylesheet"/>
                        <script src="/public/js/require.js"></script>
        	           	<script src="/public/js/config.js"></script>
                        <!--[if lt IE 9]>
							<script src="/public/js/html5shiv.min.js"></script>
							<script src="/public/js/respond.min.js"></script>
                        <![endif]-->
        				<meta http-equiv="refresh" content="2; URL=' . $url . '" />
        				<title>管理区域</title>
    				</head>
    			 	<body>
                        <div class="gw-container">
                            <div class="container-fluid">
                                <div>
                                    <div class="jumbotron clearfix alert alert-info">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
                                                <i class="fa fa-5x fa-info-circle"></i>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
                                                <h2>信息提示</h2>
                                                <p>' . $show . ' 2秒后返回指定页面！</p>
                                                <p>如果浏览器无法跳转，<a href="' . $url . '">请点击此处</a></p>
                                            </div>
                                        </div>
                                   </div>
                                   <script>
                                        require(["jquery"],function($){
                                        var h = document.documentElement.clientHeight;
                                            $(".gw-container").css("min-height",h);
                                        });
                                   </script>			
                                </div>
                            </div>
                        </div>
                        <script>require(["bootstrap"]);</script>
                    </body>
                </html>';
        echo $msg;
		exit ();
	}
	
	/**
    * 前台通用信息提示
    */
	public function get_show_msg($url, $show = '操作已成功！') {
		$msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml"><head>
				<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
				<link rel="stylesheet" href="Public/common.css" type="text/css" />
				<meta http-equiv="refresh" content="2; url=' . $url . '" />
				<title>消息提示</title>
				</head>
				<body>
				<div id="man_zone">
				  <table width="30%" border="1" align="center"  cellpadding="3" cellspacing="0" class="table" style="margin-top:100px;">
				    <tr>
				      <th align="center" style="background:#cef">信息提示</th>
				    </tr>
				    <tr>
				      <td><p>' . $show . '<br />
				      2秒后返回指定页面！<br />
				      如果浏览器无法跳转，<a href="' . $url . '">请点击此处</a>。</p></td>
				    </tr>
				  </table>
				</div>
				</body>
				</html>';
		echo $msg;
		exit ();
	}

	
	//========================
} //end class
?>














